import Vue from 'vue'
import VueRouter from 'vue-router'
import superheroes from '../views/superheroes.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/superheroes',
    name: 'superheroes',
    component: superheroes
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
